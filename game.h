#include <stdlib.h>
#include "utils.h"

#define RIGHT 1
#define LEFT 2
#define DOWN 3

#define MAX_NUMBER_OF_ENTITIES 8
#define MAX_NUMBER_OF_PROJECTILES 1000

#define HORIZONTAL_SPEED_ENTITY 3
#define VERTICAL_SPEED_ENTITY rand() % 3

//#define HORIZONTAL_SPEED_PROJECTILE 10
#define VERTICAL_SPEED_PROJECTILE 8

entity entities[MAX_NUMBER_OF_ENTITIES];
entity en_projectiles[MAX_NUMBER_OF_PROJECTILES];
entity pl_projectiles[MAX_NUMBER_OF_PROJECTILES];
entity player;
int start_of_en_proj;
int end_of_en_proj;
int start_of_pl_proj;
int end_of_pl_proj;
int move_direction;

/*
 * sets all necessary variables according to chosen difficulty
 * and starts game cycle
 * */
void start_game();

/*
 * updates location of display components and implements
 * attack logic of enemy entities
 * */
void update_status(uint32_t *player_health);

/*
 * initializes entities and player
 * their location and size is preset
 * */
void initialize_game();

/*
 * redraws location of game components into display_buffer
 * */
void paint();

/*
 * draws player visualization into display_buffer
 * */
void paint_player();

/*
 * draws visualization of enemy entities into display_buffer
 * */
void paint_enemies();

/*
 * draws visualization of projectiles entities into display_buffer
 * */
void paint_projectiles();

/*
 * initializes starting location of enemy entities
 * */
void initialize_enemies();

/*
 * moves all enemy entities in one horizontal direction with fixed speed
 * each entity moves ale vertically down, but this movement
 * is random
 * */
void enemies_move();

/*
 * picks random living entity which then produces projectile
 * which can cause damage to player
 * */
void enemies_shoot();

/*
 * if player has sufficient ammo a projectile which can
 * kill enemy entities is created and sent in vertical direction
 * towards top of the display
 * */
void player_shoot();

/*
 * moved all active projectiles in vertical direction towards the bottom of the display
 * these projectiles cause damage to player
 * */
void enemy_projectile_move();

/*
 * moved all active projectiles in vertical direction towards the top of the display
 * these projectiles kill entity in one hit
 * */
void player_projectile_move();

/*
 * initializes starting location of player
 * */
void initialize_player();

/*
 * moves player in a horizontal direction by fixed value
 * */
void player_move(int direction);

/*
 * checks if two entities intersect, if so returns true
 * returns false otherwise
 * */
_Bool intersects(entity entity1, entity entity2);

/*
 * creates new wave of enemies if all previous entities are terminated
 * */
void enemy_wave_check();

/*
 * displays score of the player into buffer
 * */
void score();

/*
 * displays ammunition status of the player
 * */
void ammunition();
