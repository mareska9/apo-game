# Galaxy shooter

## README
Project is created as semestral project for Computer Architecture.

## AUTHORS
Karel Mareš and Vít Nováček, students of [CTU](https://www.cvut.cz/en), Faculty of Electrical Engineering.

## INSTALLATION
Can be downloaded, compiled with use of Makefile and launched on [MZ_APO](https://cw.fel.cvut.cz/wiki/courses/b35apo/documentation/mz_apo/start) machine.
To compile the project use `make` command without any arguments. 
The compiled binary file called `game` can be run by linux bash.
To compile and run remotely on MZ_APO machine edit Makefile where target ip is defined and 
replace it with MZ_APO ip address, as an argument use `run`.

## LICENSE
The project is open sourced, but cannot be used as semestral project of other students.

## ABOUT GAME
The game uses 2D graphics. The main goal of this game is to shoot as many enemy ships as possible whilst maintaining 
minimal health and ammunition.
The game greets player with main menu which contains selective buttons:
* game start button
* settings button
    * the difficulty settings changes speed of the game, amount of starting ammunition and ammo drop by killing enemy entity.
* exit button

Buttons are selected by `w` and `s` keys and confirmed with `enter` key. 
The Player's ship can move in horizontal directions by using of `a` and `d` keys.
The Player's ship shoots projectiles with a press of the space bar.
For every enemy ship destroyed you get score points.
Line of LEDs shows player's current health. Two RGB LEDs on both sides are just for aesthetics.
Game ends when player has no health left or enemy ship get to the bottom of the display.
