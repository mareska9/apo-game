/*******************************************************************
  main.c      - main file

  Authors: Karel Mareš & Vít Nováček

  License: This code is open-source and can be used in any other project

 *******************************************************************/

#define _POSIX_C_SOURCE 200112L

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <termios.h>
#include <fcntl.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "serialize_lock.h"

#include "font_types.h"

#define DISPLAY_WIDTH 480
#define DISPLAY_HEIGHT 320
#define PIXEL_SIZE 2

#define PHYSICAL_MEMORY_ERROR "Error has occurred while mapping memory region\n"
#define ALLOCATION_ERROR "Error jas occurred while allocating memory\n"

#include "main_menu.h"
#include "game.h"

int main(int argc, char *argv[]) {

    /* Serialize execution of applications */

    /* Try to acquire lock the first */
    if (serialize_lock(1) <= 0) {
        printf("System is occupied\n");

        if (1) {
            printf("Waiting\n");
            /* Wait till application holding lock releases it or exits */
            serialize_lock(0);
        }
    }

    printf("Game started!\n");

    //  init of the display buffer
    display_buffer = (unsigned char *) calloc(1, DISPLAY_HEIGHT * DISPLAY_WIDTH * PIXEL_SIZE);
    if (!display_buffer) {
        fprintf(stderr, ALLOCATION_ERROR);
        return -1;
    }

    //  mapping of the display
    display = (unsigned char *) map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
    if (!display) {
        free(display_buffer);
        fprintf(stderr, PHYSICAL_MEMORY_ERROR);
        return -1;
    }

    //  mapping of other peripherals
    uint32_t *base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    if (!base) {
        free(display_buffer);
        fprintf(stderr, PHYSICAL_MEMORY_ERROR);
        return -1;
    }
    led_line = ((void *) base) + SPILED_REG_LED_LINE_o;
    set_led_line_pattern(0xffffffffu);
    rgb_left = ((void *) base) + SPILED_REG_LED_RGB1_o;
    rgb_right = ((void *) base) + SPILED_REG_LED_RGB2_o;

    //  setting font type
    fdes = &font_rom8x16;

    //
    parlcd_hx8357_init(display);

    //initialize_main_menu();

    struct termios t;

    tcgetattr(0, &t);
    t.c_lflag &= ~ICANON;
    tcsetattr(0, TCSANOW, &t);
    fcntl(0, F_SETFL, fcntl(0, F_GETFL) | O_NONBLOCK);

    //  GAME START
    start_menu();
    difficulty = DIF_MEDIUM;
    /*difficulty_time = MEDIUM_TIME;
    ammo_num = MEDIUM_AMMO;
    add_ammo = MEDIUM_AMMO_GAIN;*/

    //  CLEANING
    clear_display();
    refresh_display();
    free(display_buffer);
    set_led_line_pattern(0u);
    set_rgb_led_color(0u, 0u, 0u, rgb_left);
    set_rgb_led_color(0u, 0u, 0u, rgb_right);
    parlcd_write_cmd(display, 0x28);

    printf("Game ended!\n");
    /* Release the lock */
    serialize_unlock();

    return 0;
}
