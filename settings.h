/*
 * starts settings selection which indicates selected element
 * element can be chosen by press of enter key and elements
 * can be chosen by w and s keys
 * if back key is chosen it automatically goes back to menu
 * other selection result in difficulty setting change and
 * immediate return to start menu
 * */
void start_settings();

/*
 * selects hard option from list
 * if enter is pressed settings will apply and
 * user will be return to start menu
 * */
void select_hard();

/*
 * selects medium option from list
 * if enter is pressed settings will apply and
 * user will be return to start menu
 * */
void select_medium();

/*
 * selects easy option from list
 * if enter is pressed settings will apply and
 * user will be return to start menu
 * */
void select_easy();

/*
 * selects back option from list
 * and immediately return back to start menu
 * */
void select_back();