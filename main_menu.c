#include "game.h"
#include "main_menu.h"
#include <stdio.h>
#include "settings.h"

//#define BACKGROUND_COLOR 0x0u

#define MAIN_TEXT_X 0
#define MAIN_TEXT_Y 0
#define MAIN_TEXT_SCALE 5
#define MAIN_TEXT_TEXT "^Galaxy 1.0^"
#define MAIN_TEXT_COLOR 0xffffu

#define START_GAME_X 60
#define START_GAME_Y 85
#define START_GAME_SCALE 4
#define START_GAME_TEXT "Start game"
#define START_GAME_COLOR 0xffffu

#define SETTINGS_X 60
#define SETTINGS_Y 164
#define SETTINGS_SCALE 4
#define SETTINGS_TEXT "Settings"
#define SETTINGS_COLOR 0xffffu

#define EXIT_X 60
#define EXIT_Y 243
#define EXIT_SCALE 4
#define EXIT_TEXT "Exit"
#define EXIT_COLOR 0xffffu

void start_menu() {
    char ch;
    int status = 0;
    _Bool running = 1;
    //  GAME CYCLE
    while (running) {
        ch = getchar();
        switch (ch) {
            /* UP */
            case 'W':
            case 'w':
                status = (status - 1) % 3;
                break;
                /* DOWN */
            case 'S':
            case 's':
                status = (status + 1) % 3;
                break;
            case '\n':
                switch (status) {
                    case 0:
                        /* start game */
                        start_game();
                        break;
                    case 1:
                        /* settings */
                        start_settings();
                        break;
                    case 2:
                        /* exit */
                        running = 0;
                        break;
                    default:
                        break;
                }
                break;

            default:
                break;
        }

        if (status < 0) {
            status = 2;
        }

        switch (status) {
            case 0:
                select_start_game();
                set_led_line_pattern(0xFFE00000u);
                break;
            case 1:
                select_settings();
                set_led_line_pattern(0x1FFC00u);
                break;
            case 2:
                select_exit();
                set_led_line_pattern(0x3FFu);
                break;

            default:
                running = 0;
                break;
        }
        draw_string(MAIN_TEXT_X, MAIN_TEXT_Y, MAIN_TEXT_TEXT, MAIN_TEXT_SCALE, 0, MAIN_TEXT_COLOR);
        set_rgb_led_color(rand(), rand(), rand(), rgb_left);
        set_rgb_led_color(rand(), rand(), rand(), rgb_right);
        refresh_display(display);
        clear_display();
    }
}

//void initialize_main_menu()
//{
//    set_background(background_color);
//    draw_string(MAIN_TEXT_X, MAIN_TEXT_Y, MAIN_TEXT_TEXT, MAIN_TEXT_SCALE, 0, MAIN_TEXT_COLOR);
//    draw_string(START_GAME_X, START_GAME_Y, START_GAME_TEXT, START_GAME_SCALE, 0, START_GAME_COLOR);
//    draw_string(SETTINGS_X, SETTINGS_Y, SETTINGS_TEXT, SETTINGS_SCALE, 0, SETTINGS_COLOR);
//    draw_string(EXIT_X, EXIT_Y, EXIT_TEXT, EXIT_SCALE, 0, EXIT_COLOR);
//}

void select_start_game() {
    draw_color(0, 85, 480, 64, START_GAME_COLOR);
    draw_string(START_GAME_X, START_GAME_Y, START_GAME_TEXT, START_GAME_SCALE, 0, background_color);
    draw_color(0, 164, 480, 64, background_color);
    draw_string(SETTINGS_X, SETTINGS_Y, SETTINGS_TEXT, SETTINGS_SCALE, 0, SETTINGS_COLOR);
    draw_color(0, 243, 480, 64, background_color);
    draw_string(EXIT_X, EXIT_Y, EXIT_TEXT, EXIT_SCALE, 0, EXIT_COLOR);
}

void select_settings() {
    draw_color(0, 85, 480, 64, background_color);
    draw_string(START_GAME_X, START_GAME_Y, START_GAME_TEXT, START_GAME_SCALE, 0, START_GAME_COLOR);
    draw_color(0, 164, 480, 64, SETTINGS_COLOR);
    draw_string(SETTINGS_X, SETTINGS_Y, SETTINGS_TEXT, SETTINGS_SCALE, 0, background_color);
    draw_color(0, 243, 480, 64, background_color);
    draw_string(EXIT_X, EXIT_Y, EXIT_TEXT, EXIT_SCALE, 0, EXIT_COLOR);
}

void select_exit() {
    draw_color(0, 85, 480, 64, background_color);
    draw_string(START_GAME_X, START_GAME_Y, START_GAME_TEXT, START_GAME_SCALE, 0, START_GAME_COLOR);
    draw_color(0, 164, 480, 64, background_color);
    draw_string(SETTINGS_X, SETTINGS_Y, SETTINGS_TEXT, SETTINGS_SCALE, 0, SETTINGS_COLOR);
    draw_color(0, 243, 480, 64, EXIT_COLOR);
    draw_string(EXIT_X, EXIT_Y, EXIT_TEXT, EXIT_SCALE, 0, background_color);
}
