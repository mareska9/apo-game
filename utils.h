#include <stdint.h>
#include "font_types.h"

#define DISPLAY_WIDTH 480
#define DISPLAY_HEIGHT 320
#define PIXEL_SIZE 2

#define DIF_HARD 'H'
#define DIF_MEDIUM 'M'
#define DIF_EASY 'E'

#define HARD_TIME 0
#define MEDIUM_TIME 7
#define EASY_TIME 14

#define HARD_AMMO 5
#define MEDIUM_AMMO 10
#define EASY_AMMO 20

#define HARD_AMMO_GAIN 2
#define MEDIUM_AMMO_GAIN 3
#define EASY_AMMO_GAIN 5

//  DISPLAY SECTION

uint16_t background_color;

/*
 * entity is used as a container on display
 * and for checking intersection
 * */
typedef struct {
    int x, y, width, height;
    _Bool alive;
} entity;

unsigned char *display_buffer;
unsigned char *display;
int ammo_num;
int add_ammo;
uint8_t difficulty_time;
char difficulty;
uint32_t *led_line;
uint32_t *rgb_left;
uint32_t *rgb_right;


font_descriptor_t *fdes;

/*
 * draws pixel (of chosen color) into display_buffer
 * */
void draw_pixel(int x, int y, uint16_t color);

/*
 * copies buffer into display
 * */
void refresh_display();

/*
 * sets background of display_buffer to chosen color
 * */
void set_background(uint16_t color);

/*
 * sets display_buffer to black
 * */
void clear_display();

/*
 * draws character of chosen scale and color to chosen position
 * the position is taken as left top corner of the character
 * font has to be predefined in *fdes
 * */
void draw_char(int x, int y, char ch, uint16_t color, int scale);

/*
 * draws chosen color to chosen area of the display buffer
 * */
void draw_color(int x, int y, int width, int height, uint16_t color);

/*
 * draws chosen scaled string to chosen location
 * string has to be properly finished with '\0'
 * */
void draw_string(int x, int y, char *string, int scale, int space, uint16_t color);

/*
 * draws colored shape using uint16_t* values as mask
 * height is length of values array
 * */
void draw_shape(int x, int y, int height, uint16_t *values, int scale, uint16_t color);

/*
 * converts R, G, B components to 16 bit format
 * */
uint16_t rgb_to_lcd(int r, int g, int b);

//  LED SECTION

/*
 * sets all 32 led diodes of led line according to uint binary representation
 * */
void set_led_line_pattern(uint32_t num);

/*
 * sets chosen rgb led diode according to r g b components
 * */
void set_rgb_led_color(unsigned char r, unsigned char g, unsigned char b, void *rgb_led);

/*
 * converts hsv color representation to to rgb color representation
 * */
uint16_t hsv2rgb(uint16_t hue, uint16_t saturation, uint16_t value);

/*
 *
 * */
void blink_rgb_leds(unsigned char r, unsigned char g, unsigned char b);

