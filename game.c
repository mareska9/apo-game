#include "game.h"
#include <stdio.h>
#include <time.h>

_Bool running;
int ship_height = 10;
int score_num = 0;

uint16_t ship_en[] = {
        0x3c00,
        0x3c00,
        0xff00,
        0xff00,
        0xbd00,
        0xbd00,
        0x9900,
        0x9900,
        0x1800,
        0x1800,
};

uint16_t ship_pl[] = {
        0x1800,
        0x1800,
        0x9900,
        0x9900,
        0xbd00,
        0xbd00,
        0xff00,
        0xff00,
        0x3c00,
        0x3c00,
};

void start_game() {
    move_direction = LEFT;
    score_num = 0;
    start_of_en_proj = start_of_pl_proj = end_of_en_proj = end_of_pl_proj = 0;
    uint32_t player_health = ~0u;
    initialize_game();
    char ch;
    running = 1;
    switch (difficulty) {
        case DIF_HARD:
            difficulty_time = HARD_TIME;
            ammo_num = HARD_AMMO;
            add_ammo = HARD_AMMO_GAIN;
            break;
        case DIF_EASY:
            difficulty_time = EASY_TIME;
            ammo_num = EASY_AMMO;
            add_ammo = EASY_AMMO_GAIN;
            break;
        default:
            difficulty_time = MEDIUM_TIME;
            ammo_num = MEDIUM_AMMO;
            add_ammo = MEDIUM_AMMO_GAIN;
            break;
    }
    while (running && player_health) {
        ch = getchar();
        switch (ch) {
            /* LEFT */
            case 'A':
            case 'a':
                player_move(LEFT);
                break;
                /* RIGHT */
            case 'D':
            case 'd':
                player_move(RIGHT);
                break;
            case ' ':
                player_shoot();
                break;
            case 'p':
                running = 0;
            default:
                break;
        }
        update_status(&player_health);
        struct timespec loop_delay = {.tv_sec = 0, .tv_nsec = difficulty_time * 1000 * 1000};
        clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
    }
    clear_display();
    char buffer[45];
    sprintf(buffer, "Your score is %d", score_num);
    draw_string(0, 50, "Congratulations!", 3, 0, 0xffu);
    draw_string(0, 100, buffer, 3, 0, 0xffu);
    refresh_display();
    struct timespec end = {.tv_sec = 5, .tv_nsec = 0};
    clock_nanosleep(CLOCK_MONOTONIC, 0, &end, NULL);
}

void update_status(uint32_t *player_health) {
    set_background(background_color);
    player_projectile_move();
    enemy_projectile_move();
    enemies_move();
    enemy_wave_check();


    if (rand() % 100 < 7)
        enemies_shoot();
    for (int i = 0; i < MAX_NUMBER_OF_PROJECTILES; i++) {
        if (en_projectiles[i].alive && intersects(player, en_projectiles[i])) {
            en_projectiles[i].alive = 0;
            (*player_health) <<= 1;
        }
    }

    for (int i = 0; i < MAX_NUMBER_OF_PROJECTILES; i++) {
        for (int j = 0; j < MAX_NUMBER_OF_ENTITIES; j++) {
            if (pl_projectiles[i].alive && entities[j].alive && intersects(entities[j], pl_projectiles[i])) {
                entities[j].alive = 0;
                pl_projectiles[i].alive = 0;
                score_num++;
                ammo_num += add_ammo;
            }
        }
    }

    paint();
    refresh_display();
    set_led_line_pattern(*player_health);
}

void initialize_game() {
    set_background(background_color);
    initialize_player();
    initialize_enemies();
}

void initialize_player() {
    player.height = 30;
    player.width = 48;
    player.x = 216;
    player.y = 250;
}

void initialize_enemies() {
    for (int row = 0; row < 2; row++) {
        for (int column = 0; column < 4; column++) {
            entities[row * 4 + column].alive = 1;
            entities[row * 4 + column].height = 48;
            entities[row * 4 + column].width = 48;
            entities[row * 4 + column].x = 60 * (row + 1) + ((entities[row * 4 + column].width * 2) * column);
            entities[row * 4 + column].y = -120 * (row + 1);
        }
    }
}

void paint() {
    set_background(background_color);
    paint_player();
    paint_enemies();
    paint_projectiles();
    score();
    ammunition();
}

void paint_player() {
    draw_shape(player.x, player.y, ship_height, ship_pl, 6, 0xffu);
}

void paint_enemies() {
    for (int i = 0; i < MAX_NUMBER_OF_ENTITIES; i++) {
        if (entities[i].alive) {
            draw_shape(entities[i].x, entities[i].y, ship_height, ship_en, 6, 0xffffu);
        }
    }
}

void paint_projectiles() {
    for (int i = 0; i < MAX_NUMBER_OF_PROJECTILES; i++) {
        if (en_projectiles[i].alive) {
            draw_color(en_projectiles[i].x, en_projectiles[i].y, en_projectiles[i].width, en_projectiles[i].height,
                       0xffu);
        }
    }

    for (int i = 0; i < MAX_NUMBER_OF_PROJECTILES; i++) {
        if (pl_projectiles[i].alive) {
            draw_color(pl_projectiles[i].x, pl_projectiles[i].y, pl_projectiles[i].width, pl_projectiles[i].height,
                       0xffu);
        }
    }
}


void enemies_move() {
    if (move_direction == LEFT) {
        for (int i = 0; i < MAX_NUMBER_OF_ENTITIES; ++i) {
            if (entities[i].alive && entities[i].x <= 0) {
                move_direction = RIGHT;
            } else {
                entities[i].x -= HORIZONTAL_SPEED_ENTITY;
            }
        }
    } else if (move_direction == RIGHT) {
        for (int i = 0; i < MAX_NUMBER_OF_ENTITIES; ++i) {
            if (entities[i].alive && (entities[i].x + entities[i].width) >= DISPLAY_WIDTH) {
                move_direction = LEFT;
            } else {
                entities[i].x += HORIZONTAL_SPEED_ENTITY;
            }
        }
    }
    for (int i = 0; i < MAX_NUMBER_OF_ENTITIES; ++i) {
        entities[i].y += VERTICAL_SPEED_ENTITY;
        if (entities[i].alive && entities[i].y + entities[i].height >= DISPLAY_HEIGHT) {
            running = 0;
        }
    }
}

void player_move(int direction) {
    if (direction == LEFT && ((player.x - 24) >= 0)) {
        if (player.x - 24 <= 0) { player.x = 0; }
        else { player.x -= 24; }
    } else if (direction == RIGHT && ((player.x + player.height + 24) <= 480)) { player.x += 24; }
}

void enemies_shoot() {
    int enemy_pos = rand() % MAX_NUMBER_OF_ENTITIES;
    for (uint16_t i = 0; i < MAX_NUMBER_OF_ENTITIES; i++) {
        if (entities[enemy_pos].alive) {
            entity projectile = {.alive = 1, .height = 30, .width = 10, .x = entities[enemy_pos].x +
                                 ((entities[enemy_pos].width / 2) - ((en_projectiles[enemy_pos].width) / 2)),
                                 .y = entities[enemy_pos].y + entities[enemy_pos].height + 31};
            en_projectiles[end_of_en_proj] = projectile;
            end_of_en_proj = (end_of_en_proj + 1) % MAX_NUMBER_OF_PROJECTILES;
            break;
        }
        enemy_pos = (enemy_pos + 1) % MAX_NUMBER_OF_ENTITIES;
    }
}

void player_shoot() {
    if (ammo_num > 0) {
        entity projectile = {.alive = 1, .height = 30, .width = 10, .x= ((player.width / 2)
                             - ((pl_projectiles[end_of_pl_proj].width) / 2)) + player.x, .y = player.y - 31};
        pl_projectiles[end_of_pl_proj] = projectile;
        end_of_pl_proj = (end_of_pl_proj + 1) % MAX_NUMBER_OF_PROJECTILES;
        ammo_num--;
    }
}

void enemy_projectile_move() {
    for (int i = 0; i < MAX_NUMBER_OF_PROJECTILES; i++) {
        if (en_projectiles[i].alive) {
            en_projectiles[i].y += VERTICAL_SPEED_PROJECTILE;
        }

        if (en_projectiles[i].y > DISPLAY_HEIGHT + en_projectiles[i].height) {
            en_projectiles[i].alive = 0;
        }
    }
}

void player_projectile_move() {
    for (int i = 0; i < MAX_NUMBER_OF_PROJECTILES; i++) {
        if (pl_projectiles[i].alive) {
            pl_projectiles[i].y -= VERTICAL_SPEED_PROJECTILE;
        }
        if (pl_projectiles[i].y < -pl_projectiles[i].height) {
            pl_projectiles[i].alive = 0;
        }
    }
}

void enemy_wave_check() {
    int non_alive_count = 0;
    for (int i = 0; i < MAX_NUMBER_OF_ENTITIES; ++i) {
        if (!entities[i].alive) {
            non_alive_count++;
        }
    }
    if (non_alive_count == MAX_NUMBER_OF_ENTITIES) {
        initialize_enemies();
    }
}

void score() {
    char buffer[50];
    sprintf(buffer, "SCORE: %d", score_num);
    draw_string(0, 5, buffer, 3, 0, 0xffu);
}

void ammunition() {
    char buffer[50];
    sprintf(buffer, "AMMO: %d", ammo_num);
    draw_string(0, 280, buffer, 3, 0, 0xffu);
}

_Bool intersects(entity entity1, entity entity2) {
    if (((entity1.x <= entity2.x) && (entity1.x + entity1.width >= entity2.x)
        && (entity1.y <= (entity2.y + entity2.height)) && (entity1.y + entity1.height >= (entity2.y + entity2.height)))
         || ((entity1.x <= (entity2.x + entity2.width)) && (entity1.x + entity1.width >= (entity2.x + entity2.width))
         && (entity1.y <= (entity2.y + entity2.height)) && (entity1.y + entity1.height >= (entity2.y + entity2.height)))
         || ((entity1.x <= (entity2.x + entity2.width)) && (entity1.x + entity1.width >= (entity2.x + entity2.width))
         && ((entity1.y <= entity2.y) && (entity1.y + entity1.height >= entity2.y)))
         || ((entity1.x <= entity2.x) && (entity1.x + entity1.width >= entity2.x) && (entity1.y <= entity2.y)
         && (entity1.y + entity1.height >= entity2.y))
         || ((entity2.x <= entity1.x) && (entity2.x + entity2.width >= entity1.x)
         && (entity2.y <= (entity1.y + entity1.height)) && (entity2.y + entity2.height >= (entity1.y + entity1.height)))
         || ((entity2.x <= (entity1.x + entity1.width)) && (entity2.x + entity2.width >= (entity1.x + entity1.width))
         && (entity2.y <= (entity1.y + entity1.height)) && (entity2.y + entity2.height >= (entity1.y + entity1.height)))
         || ((entity1.x <= (entity2.x + entity2.width)) && (entity1.x + entity1.width >= (entity2.x + entity2.width))
         && ((entity1.y <= entity2.y) && (entity1.y + entity1.height >= entity2.y)))
         || ((entity2.x <= entity1.x) && (entity2.x + entity2.width >= entity1.x) && (entity2.y <= entity1.y)
         && (entity2.y + entity2.height >= entity1.y))) {
        blink_rgb_leds(255, 0, 0);
        blink_rgb_leds(0, 0, 0);
        return 1;
    }
    return 0;
}
