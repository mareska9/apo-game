/*
 * initializes start menu
 * creates selection list
 * selection and confirmation with selection results in adequate response
 * */
void start_menu();

/*
 * calls start_game() function
 * */
void select_start_game();

/*
 * calls start_game() function
 * */
void select_settings();

/*
 * stops entire game, frees allocated memory and cleans peripherals
 * */
void select_exit();