#include <stdlib.h>
#include "mzapo_parlcd.h"
#include "utils.h"
//#include <time.h>

//  DISPLAY

void draw_pixel(int x, int y, uint16_t color) {
    if (x >= 0 && x <= DISPLAY_WIDTH && y >= 0 && y <= DISPLAY_HEIGHT) {
        display_buffer[x + y * DISPLAY_WIDTH] = color;
    }
}

void refresh_display() {
    parlcd_write_cmd(display, 0x2c);
    for (size_t i = 0; i < DISPLAY_HEIGHT * DISPLAY_WIDTH; i++) {
        parlcd_write_data(display, display_buffer[i]);
    }
}

void clear_display() {
    for (size_t i = 0; i < DISPLAY_HEIGHT * DISPLAY_WIDTH; i++) {
        display_buffer[i] = 0u;
    }
}

void draw_char(int x, int y, char ch, uint16_t color, int scale) {
    int width = fdes->maxwidth;
    const font_bits_t *ptr;
    if (ch >= fdes->firstchar && (ch - fdes->firstchar < fdes->size) && scale > 0) {
        if (fdes->offset) {
            ptr = &fdes->bits[fdes->offset[ch - fdes->firstchar]];
        } else {
            int bw = (fdes->maxwidth + 15) / 16;
            ptr = &fdes->bits[(ch - fdes->firstchar) * bw * fdes->height];
        }
        for (size_t i = 0; i < fdes->height * scale; i += scale) {
            font_bits_t val = *ptr;
            for (size_t j = 0; j < width * scale; j += scale) {
                if ((val & 0x8000) != 0) {
                    draw_color(x + j, y + i, scale, scale, color);
                }
                val <<= 1;
            }
            ptr++;
        }
    }
}

void draw_color(int x, int y, int width, int height, uint16_t color) {
    for (size_t i = 0; i < height; i++) {
        for (size_t j = 0; j < width; j++) {
            draw_pixel(x + j, y + i, color);
        }
    }
}

void draw_string(int x, int y, char *string, int scale, int space, uint16_t color) {
    if (string && scale > 0) {
        char c;
        uint16_t offset = 0;
        while ((c = (*string++))) {
            draw_char(x + offset, y, c, color, scale);
            offset += fdes->maxwidth * scale + space;
        }
    }
}


void set_background(uint16_t color) {
    for (size_t i = 0; i < DISPLAY_HEIGHT * DISPLAY_WIDTH; i++) {
        display_buffer[i] = color;
    }
}

void draw_shape(int x, int y, int height, uint16_t *values, int scale, uint16_t color) {
    for (size_t i = 0; i < height * scale; i += scale) {
        uint16_t val = *values;
        for (size_t j = 0; j < 16 * scale; j += scale) {
            if ((val & 0x8000) != 0) {
                draw_color(x + j, y + i, scale, scale, color);
            }
            val <<= 1;
        }
        values++;
    }
}

uint16_t rgb_to_lcd(int r, int g, int b) {
    r >>= 3;
    g >>= 2;
    b >>= 3;
    return (((r & 0x1f) << 11) | ((g & 0x3f) << 5) | (b & 0x1f));
}

//  LEDS

void set_led_line_pattern(uint32_t num) {
    *led_line = num;
}

void set_rgb_led_color(unsigned char r, unsigned char g, unsigned char b, void *rgb_led) {
    *(unsigned char *) rgb_led = r;
    *((unsigned char *) rgb_led + 1) = g;
    *((unsigned char *) rgb_led + 2) = b;
}

uint16_t hsv2rgb(uint16_t hue, uint16_t saturation, uint16_t value) {
    hue = hue % 360;
    float f = ((hue % 60) / 60.0);
    int p = (value * (255 - saturation)) / 255;
    int q = (value * (255 - saturation * f)) / 255;
    int t = (value * (255 - saturation * (1.0 - f))) / 255;
    unsigned int r, g, b;
    if (hue < 60) {
        r = value;
        g = t;
        b = p;
    } else if (hue < 120) {
        r = q;
        g = value;
        b = p;
    } else if (hue < 180) {
        r = p;
        g = value;
        b = t;
    } else if (hue < 240) {
        r = p;
        g = q;
        b = value;
    } else if (hue < 300) {
        r = t;
        g = p;
        b = value;
    } else {
        r = value;
        g = p;
        b = q;
    }
    return ((r << 16) | (g << 8) | b);
}

void blink_rgb_leds(unsigned char r, unsigned char g, unsigned char b) {
    set_rgb_led_color(r, g, b, rgb_left);
    set_rgb_led_color(r, g, b, rgb_right);
}
