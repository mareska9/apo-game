#include "settings.h"
#include "utils.h"
#include <stdio.h>

#define HARD_X 60
#define HARD_Y 6
#define HARD_SCALE 4
#define HARD_TEXT_TEXT "Hard"
#define HARD_TEXT_COLOR 0xffffu

#define MEDIUM_X 60
#define MEDIUM_Y 85
#define MEDIUM_SCALE 4
#define MEDIUM_TEXT "Medium"
#define MEDIUM_COLOR 0xffffu

#define EASY_X 60
#define EASY_Y 164
#define EASY_SCALE 4
#define EASY_TEXT "Easy"
#define EASY_COLOR 0xffffu

#define BACK_X 60
#define BACK_Y 243
#define BACK_SCALE 4
#define BACK_TEXT "Back"
#define BACK_COLOR 0xffffu

void start_settings() {
    char ch;
    int status = 0;
    _Bool running = 1;
    //  GAME CYCLE
    while (running) {
        ch = getchar();
        switch (ch) {
            /* UP */
            case 'W':
            case 'w':
                status = (status - 1) % 4;
                break;
                /* DOWN */
            case 'S':
            case 's':
                status = (status + 1) % 4;
                break;
            case '\n':
                switch (status) {
                    case 0:
                        difficulty = DIF_HARD;
                        break;
                    case 1:
                        difficulty = DIF_MEDIUM;
                        break;
                    case 2:
                        difficulty = DIF_EASY;
                        break;
                    default:
                        break;
                }
                running = 0;
                break;

            default:
                break;
        }

        if (status < 0) {
            status = 3;
        }

        switch (status) {
            case 0:
                select_hard();
                set_led_line_pattern(0xF000u);
                break;
            case 1:
                select_medium();
                set_led_line_pattern(0xF00u);
                break;
            case 2:
                select_easy();
                set_led_line_pattern(0xF0u);
                break;
            case 3:
                select_back();
                set_led_line_pattern(0xFu);
            default:
                running = 0;
                break;
        }
        refresh_display(display);
        clear_display();
    }
}

void select_hard() {
    draw_color(0, 6, 480, 64, HARD_TEXT_COLOR);
    draw_string(HARD_X, HARD_Y, HARD_TEXT_TEXT, HARD_SCALE, 0, background_color);
    draw_color(0, 85, 480, 64, background_color);
    draw_string(MEDIUM_X, MEDIUM_Y, MEDIUM_TEXT, MEDIUM_SCALE, 0, MEDIUM_COLOR);
    draw_color(0, 164, 480, 64, background_color);
    draw_string(EASY_X, EASY_Y, EASY_TEXT, EASY_SCALE, 0, EASY_COLOR);
    draw_color(0, 243, 480, 64, background_color);
    draw_string(BACK_X, BACK_Y, BACK_TEXT, BACK_SCALE, 0, BACK_COLOR);
}

void select_medium() {
    draw_color(0, 6, 480, 64, background_color);
    draw_string(HARD_X, HARD_Y, HARD_TEXT_TEXT, HARD_SCALE, 0, HARD_TEXT_COLOR);
    draw_color(0, 85, 480, 64, MEDIUM_COLOR);
    draw_string(MEDIUM_X, MEDIUM_Y, MEDIUM_TEXT, MEDIUM_SCALE, 0, background_color);
    draw_color(0, 164, 480, 64, background_color);
    draw_string(EASY_X, EASY_Y, EASY_TEXT, EASY_SCALE, 0, EASY_COLOR);
    draw_color(0, 243, 480, 64, background_color);
    draw_string(BACK_X, BACK_Y, BACK_TEXT, BACK_SCALE, 0, BACK_COLOR);
}

void select_easy() {
    draw_color(0, 6, 480, 64, background_color);
    draw_string(HARD_X, HARD_Y, HARD_TEXT_TEXT, HARD_SCALE, 0, HARD_TEXT_COLOR);
    draw_color(0, 85, 480, 64, background_color);
    draw_string(MEDIUM_X, MEDIUM_Y, MEDIUM_TEXT, MEDIUM_SCALE, 0, MEDIUM_COLOR);
    draw_color(0, 164, 480, 64, EASY_COLOR);
    draw_string(EASY_X, EASY_Y, EASY_TEXT, EASY_SCALE, 0, background_color);
    draw_color(0, 243, 480, 64, background_color);
    draw_string(BACK_X, BACK_Y, BACK_TEXT, BACK_SCALE, 0, BACK_COLOR);
}

void select_back() {
    draw_color(0, 6, 480, 64, background_color);
    draw_string(HARD_X, HARD_Y, HARD_TEXT_TEXT, HARD_SCALE, 0, HARD_TEXT_COLOR);
    draw_color(0, 85, 480, 64, background_color);
    draw_string(MEDIUM_X, MEDIUM_Y, MEDIUM_TEXT, MEDIUM_SCALE, 0, MEDIUM_COLOR);
    draw_color(0, 164, 480, 64, background_color);
    draw_string(EASY_X, EASY_Y, EASY_TEXT, EASY_SCALE, 0, EASY_COLOR);
    draw_color(0, 164, 480, 64, BACK_COLOR);
    draw_string(BACK_X, BACK_Y, BACK_TEXT, BACK_SCALE, 0, background_color);
}